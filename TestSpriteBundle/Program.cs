﻿using System;
using System.IO;
using System.IO.Compression;
using System.Text;
using dk.zunk.SpriteBundle;

namespace TestSpriteBundle
{
  internal static class Program
  {
    private static void Main()
    {
      const string root = "../../images";
      var css = File.ReadAllText(Path.Combine(root, "sprite.css"));
      string mime;
      var resultCss = SpriteFromCss.Create(css, false, out mime, rootPath: root);
      Console.WriteLine("Write {0}..", mime);
      File.WriteAllText("test.css", resultCss, Encoding.UTF8);
      Console.WriteLine("Style size:   {0,5} bytes", new FileInfo("test.css").Length);
      Console.WriteLine("GZipped size: {0,5} bytes", GetGZippedLength(resultCss));

      Console.WriteLine();

      resultCss = SpriteFromCss.Create(css, true, out mime, rootPath: root);
      Console.WriteLine("Write {0}..", mime);
      File.WriteAllText("test_webp.css", resultCss, Encoding.UTF8);
      Console.WriteLine("Style size:   {0,5} bytes", new FileInfo("test_webp.css").Length);
      Console.WriteLine("GZipped size: {0,5} bytes", GetGZippedLength(resultCss));
    }

    private static long GetGZippedLength(string str)
    {
      var memoryStream = new MemoryStream(30 * 1024);
      new StreamWriter(new GZipStream(memoryStream, CompressionLevel.Optimal), Encoding.UTF8).Write(str);
      return memoryStream.Position;
    }
  }
}
