﻿<%@ Page Language="C#" %>
<%@ Import Namespace="System.Web.Optimization" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat="server">

</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
  <title>Example of SpriteBundle</title>
  <link rel="stylesheet" type="text/css" href="/bundles/sprite" />
  <%= Styles.Render("~/bundles/sprite") %>
</head>
<body>
  <form id="HtmlForm" runat="server">
    <div>
      <div class="sprite btn_default_selected"></div>
      <div class="sprite btn_dialog_pressed"></div>
      <div class="sprite btn_rating_star_on_pressed"></div>
      <div class="sprite btn_zoom_page_normal"></div>
      <div class="sprite ic_menu_account_list"></div>
      <div class="sprite ic_menu_btn_add"></div>
      <div class="sprite ic_menu_call"></div>
      <div class="sprite ic_menu_camera"></div>
    </div>
  </form>
</body>
</html>
