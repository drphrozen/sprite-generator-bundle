﻿using System.Web.Optimization;
using dk.zunk.SpriteBundle;

namespace ExampleSpriteBundle
{
  public static class BundleConfig
  {
    public static void RegisterBundles(BundleCollection bundles)
    {
      bundles.Add(new SpriteBundle("~/bundles/sprite").Include("~/styles/sprite.css"));
    }
  }
}