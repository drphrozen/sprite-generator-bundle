﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Imazen.WebP;
using SpriteGenerator;

namespace dk.zunk.SpriteBundle
{
  public static class SpriteFromCss
  {
    private static readonly int WebpVersion;

    static SpriteFromCss()
    {
      try
      {
        WebpVersion = Imazen.WebP.Extern.NativeMethods.WebPGetEncoderVersion();
      }
      catch (DllNotFoundException)
      {

      }
    }

    public static string Create(string css, bool tryWebp, out string mime, string rootPath = null, Sprite sprite = null)
    {
      const string escaped = @"'(?<url>[^'\\]*(?:\\.[^'\\]*)*)'";
      var uriMatch = string.Format(@"(?:{0}|{1}|{2})", escaped, escaped.Replace('\'', '"'), "(?<url>[^'\")].*?)");
      var regex = new Regex(string.Format(@"background-image:\s*url\(\s*{0}\s*\)", uriMatch),
                            RegexOptions.IgnoreCase);
      var backgroundImages = regex.Matches(css);
      var results = backgroundImages
        .Cast<Match>()
        .Select(match => new MatchResult { Match = match, Url = match.Groups["url"].Value })
        .ToArray();

      var sb = new StringBuilder(css.Length);

      var files = results.Select((result, index) => new { result, index })
        .Where(x => !string.IsNullOrEmpty(x.result.Url))
        .Select(x => new { x.index, stream = new FilePathStream(x.result.Url, rootPath) })
        .ToArray();

      if (sprite == null)
        sprite = new Sprite();

      Bitmap image;
      {
        var rectangles = sprite.Create(files.Select(x => x.stream), out image).ToList();
        for (var index = 0; index < files.Length; index++)
        {
          var file = files[index];
          results[file.index].Rectangle = rectangles[index];
        }
      }

      var bytes = new byte[32 * 1024];
      var memoryStream = new MemoryStream(bytes);
      var isWebp = false;
      if (tryWebp)
      {
        isWebp = TryWebp(image, memoryStream);
      }
      if(!isWebp)
        image.Save(memoryStream, ImageFormat.Png);

      var i = 0;
      foreach (var result in results)
      {
        var match = result.Match;
        var rect = result.Rectangle;
        if (i < match.Index)
        {
          var length = match.Index - i;
          sb.Append(css.Substring(i, length));
          i += length;
        }
        i += match.Length;
        if (!string.IsNullOrEmpty(result.Url))
        {
          sb.AppendFormat("width: {0}px; height: {1}px; background-position: {2}px {3}px", rect.Width, rect.Height, rect.X, rect.Y);
        }
        else
        {
          sb.AppendFormat("background-image: url({0})", string.Format(@"data:image/png;base64,{0}", Convert.ToBase64String(bytes, 0, (int)memoryStream.Position)));
        }
      }
      if (i < css.Length)
        sb.Append(css.Substring(i, css.Length - i));
      mime = isWebp ? "image/webp" : "image/png";
      return sb.ToString();
    }

    private static bool TryWebp(Bitmap bitmap, Stream stream)
    {
      if (WebpVersion < 1) return false;
      var simpleEncoder = new SimpleEncoder();
      simpleEncoder.Encode(bitmap, stream, -1f);
      return true;
    }
  }

  internal class MatchResult
  {
    public Match Match { get; set; }
    public string Url { get; set; }
    public Rectangle Rectangle { get; set; }
  }
}