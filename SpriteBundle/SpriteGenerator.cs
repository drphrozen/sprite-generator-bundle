﻿using System;
using System.Web.Optimization;

namespace dk.zunk.SpriteBundle
{
  public class SpriteGenerator : IBundleTransform
  {
    private const string CssContentType = "text/css";

    public void Process(BundleContext context, BundleResponse response)
    {
      if (context == null)
        throw new ArgumentNullException("context");
      if (response == null)
        throw new ArgumentNullException("response");
      if (!context.EnableInstrumentation)
      {
        var mime = (string) context.HttpContext.Items[SpriteBundle.MimeTypeKey];
        var tryWebp = mime == "image/webp";
        response.Content = SpriteFromCss.Create(response.Content, tryWebp, out mime);
        context.HttpContext.Items[SpriteBundle.MimeTypeKey] = mime;
      }
      response.ContentType = CssContentType;
    }
  }
}
