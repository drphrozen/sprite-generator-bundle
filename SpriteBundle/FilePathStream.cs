﻿using System.IO;
using System.Web.Hosting;
using SpriteGenerator;

namespace dk.zunk.SpriteBundle
{
  public class FilePathStream : IInputStream
  {
    private readonly string _relativeUrl;
    private readonly string _rootPath;

    public FilePathStream(string relativeUrl, string rootPath = null)
    {
      _relativeUrl = relativeUrl;
      _rootPath = rootPath;
    }

    public Stream Open()
    {
      return HostingEnvironment.VirtualPathProvider != null
        ? HostingEnvironment.VirtualPathProvider.GetFile(_relativeUrl).Open()
        : File.OpenRead(_rootPath != null ? Path.Combine(_rootPath, _relativeUrl) : _relativeUrl);
    }
  }
}