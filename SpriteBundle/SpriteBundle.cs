﻿using System.Web;
using System.Web.Optimization;

namespace dk.zunk.SpriteBundle
{
  public class SpriteBundle : Bundle
  {
    public const string MimeTypeKey = "SpriteGeneratorMime";

    public SpriteBundle(string virtualPath)
      : base(virtualPath, null, new IBundleTransform[] { new SpriteGenerator() })
    {
    }

    public override BundleResponse ApplyTransforms(BundleContext context, string bundleContent, System.Collections.Generic.IEnumerable<BundleFile> bundleFiles)
    {
      var httpContext = context.HttpContext;
      httpContext.Items[MimeTypeKey] = IsWebpEnabled(httpContext)
        ? "image/webp"
        : "image/png";
      return base.ApplyTransforms(context, bundleContent, bundleFiles);
    }

    public override string GetCacheKey(BundleContext context)
    {
      return base.GetCacheKey(context) + ":" + context.HttpContext.Items["SpriteGeneratorMime"];
    }

    private static bool IsWebpEnabled(HttpContextBase context)
    {
      var httpRequest = context.Request;
      var browser = httpRequest.Browser;

      // http://caniuse.com/webp
      switch (browser.Browser)
      {
        case "Chrome":
          return browser.MajorVersion >= 9;
        case "Android":
          return browser.MajorVersion >= 4;
        case "Opera":
          return browser.MajorVersion >= 12;
      }
      return false;
    }
  }
}
