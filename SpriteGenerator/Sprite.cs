﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using SpriteGenerator.Utility;

namespace SpriteGenerator
{
  public class Sprite
  {
    public Layout Layout { get; set; }
    public int Padding { get; set; }
    public int Margin { get; set; }
    public int ImagesInRow { get; set; }
    public int ImagesInColumn { get; set; }

    public IEnumerable<Rectangle> Create(IEnumerable<IInputStream> files, out Bitmap image)
    {
      var images = CreateImages(files);

      switch (Layout)
      {
        case Layout.Automatic:
          return GenerateAutomaticLayout(images, out image);
        case Layout.Horizontal:
          return GenerateHorizontalLayout(images, out image);
        case Layout.Vertical:
          return GenerateVerticalLayout(images, out image);
        case Layout.Rectangular:
          return GenerateRectangularLayout(images, out image);
        default:
          throw new ArgumentOutOfRangeException();
      }
    }

    private static IEnumerable<Image> CreateImages(IEnumerable<IInputStream> files)
    {
      return files.Select(x =>
                            {
                              using (var stream = x.Open())
                              {
                                return Image.FromStream(stream);
                              }
                            });
    }

    private static IEnumerable<Module> CreateModules(IEnumerable<Image> images, int whiteSpace)
    {
      return images.Select((image, i) => new Module(i, image, whiteSpace));
    }

    //Automatic layout
    private IEnumerable<Rectangle> GenerateAutomaticLayout(IEnumerable<Image> images, out Bitmap image)
    {
      var sortedByArea = from m in CreateModules(images, Padding)
                         orderby m.Width*m.Height descending
                         select m;
      var moduleList = sortedByArea.ToList();
      var placement = Algorithm.Greedy(moduleList);

      //Creating an empty result image.
      Bitmap resultSprite = new Bitmap(placement.Width - Padding + 2 * Margin,
                                      placement.Height - Padding + 2 * Margin);
      var graphics = Graphics.FromImage(resultSprite);

      var list = new List<Rectangle>();

      //Drawing images into the result image in the original order and writing CSS lines.
      foreach (var m in placement.Modules)
      {
        m.Draw(graphics, Margin);
        var rectangle = new Rectangle(resultSprite.Width- (m.X + Margin), resultSprite.Height - (m.Y + Margin),
                                      m.Width - Padding,
                                      m.Height - Padding);
        list.Add(rectangle);
      }

      image = resultSprite;
      return list;
    }

    //Horizontal layout
    private IEnumerable<Rectangle> GenerateHorizontalLayout(IEnumerable<Image> images, out Bitmap image)
    {
      //TODO: fix height + width
      //Calculating result image dimension.
      var imgs = images as Image[] ?? images.ToArray();
      var width = imgs.Sum(img => img.Width + Padding);
      width = width - Padding + 2*Margin;
      var height = imgs.First().Height + 2 * Margin;

      //Creating an empty result image.
      Bitmap resultSprite = new Bitmap(width, height);
      var graphics = Graphics.FromImage(resultSprite);

      //Initial coordinates.
      var actualXCoordinate = Margin;
      var yCoordinate = Margin;

      //Drawing images into the result image, writing CSS lines and increasing X coordinate.
      var list = new List<Rectangle>(imgs.Length);
      foreach (var img in imgs)
      {
        var rectangle = new Rectangle(actualXCoordinate, yCoordinate, img.Width, img.Height);
        list.Add(rectangle);
        graphics.DrawImage(img, rectangle);
        actualXCoordinate += img.Width + Padding;
      }

      image = resultSprite;
      return list;
    }

    //Vertical layout
    private IEnumerable<Rectangle> GenerateVerticalLayout(IEnumerable<Image> images, out Bitmap image)
    {
      //TODO: fix height + width
      //Calculating result image dimension.
      var imgs = images as Image[] ?? images.ToArray();
      var height = imgs.Sum(img => img.Height + Padding);
      height = height - Padding + 2*Margin;
      var width = imgs.First().Width + 2*Margin;

      //Creating an empty result image.
      Bitmap resultSprite = new Bitmap(width, height);
      var graphics = Graphics.FromImage(resultSprite);

      //Initial coordinates.
      var actualYCoordinate = Margin;
      var xCoordinate = Margin;

      //Drawing images into the result image, writing CSS lines and increasing Y coordinate.
      var list = new List<Rectangle>(imgs.Length);
      foreach (var img in imgs)
      {
        var rectangle = new Rectangle(xCoordinate, actualYCoordinate, img.Width, img.Height);
        graphics.DrawImage(img, rectangle);
        actualYCoordinate += img.Height + Padding;
      }

      image = resultSprite;
      return list;
    }

    private IEnumerable<Rectangle> GenerateRectangularLayout(IEnumerable<Image> images, out Bitmap image)
    {
      //TODO: fix height + width
      //Calculating result image dimension.
      var imgs = images as Image[] ?? images.ToArray();
      var imageWidth = imgs.First().Width;
      var imageHeight = imgs.First().Height;
      var width = ImagesInRow*(imageWidth + Padding) -
                  Padding + 2*Margin;
      var height = ImagesInColumn*(imageHeight + Padding) -
                   Padding + 2*Margin;

      //Creating an empty result image.
      Bitmap resultSprite = new Bitmap(width, height);
      var graphics = Graphics.FromImage(resultSprite);

      //Initial coordinates.
      var actualYCoordinate = Margin;
      var actualXCoordinate = Margin;

      //Drawing images into the result image, writing CSS lines and increasing coordinates.
      var list = new List<Rectangle>(imgs.Length);
      for (var i = 0; i < ImagesInColumn; i++)
      {
        for (var j = 0; (i * ImagesInRow) + j < imgs.Length && j < ImagesInRow; j++)
        {
          var rectangle = new Rectangle(actualXCoordinate, actualYCoordinate, imageWidth, imageHeight);
          graphics.DrawImage(imgs[i*ImagesInRow + j], rectangle);
          actualXCoordinate += imageWidth + Padding;
          list.Add(rectangle);
        }
        actualYCoordinate += imageHeight + Padding;
        actualXCoordinate = Margin;
      }

      image = resultSprite;
      return list;
    }
  }
}