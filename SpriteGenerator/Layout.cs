﻿namespace SpriteGenerator
{
  public enum Layout
  {
    Automatic,
    Horizontal,
    Vertical,
    Rectangular
  }
}