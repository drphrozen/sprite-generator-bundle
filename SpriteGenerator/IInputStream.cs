﻿using System.IO;

namespace SpriteGenerator
{
  public interface IInputStream
  {
    Stream Open();
  }
}